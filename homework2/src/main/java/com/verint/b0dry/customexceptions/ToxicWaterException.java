package com.verint.b0dry.customexceptions;

public class ToxicWaterException extends Exception{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final String message;
	
	public ToxicWaterException (String message) {
		this.message = message;
		System.out.println(this.message);
	}
}
