package com.verint.b0dry.fishes;

import com.verint.b0dry.customexceptions.ToxicWaterException;

public final class Sharks extends Fish {

	public Sharks(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	protected final String fishSpeed = "fast";
	protected final String kind = "shark";
	protected final String waterType = "salt";
	
	@Override
	public void swim(String name) {

		System.out.println(fishSpeed);
	}

	@Override
	public String dive (String isTheWaterClean) throws ToxicWaterException {
		return waterCheck(isTheWaterClean, this.kind, this.waterType);
	}

	

}
