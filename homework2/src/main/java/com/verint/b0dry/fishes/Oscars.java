package com.verint.b0dry.fishes;

import com.verint.b0dry.customexceptions.ToxicWaterException;

public final class Oscars extends Fish{

	public Oscars(String name) {
		super(name);
		// TODO Auto-generated constructor stub
	}

	protected final String fishSpeed = "Fast";
	protected final String kind = "Oscar";
	protected final String waterType = "fresh";
	
	@Override
	public void swim(String name) {

		System.out.println(fishSpeed);
	}

	@Override
	public String dive (String isTheWaterClean) throws ToxicWaterException {
		return waterCheck(isTheWaterClean, this.kind, this.waterType);
	}

	

}
