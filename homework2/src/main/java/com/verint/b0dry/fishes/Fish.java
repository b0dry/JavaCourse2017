package com.verint.b0dry.fishes;

import com.verint.b0dry.customexceptions.ToxicWaterException;

public abstract class Fish {
	
	protected String fishName;
	
	public Fish (String name) {
		this.fishName = name;
	}

	public String getFishName() {
		return fishName;
	}
	public void setFishName(String fishName) {
		this.fishName = fishName;
	}
	
	public abstract void swim (String name);
	public abstract String dive (String isTheWaterClean) throws ToxicWaterException ;
	public String waterCheck(String isTheWaterClean, String name, String waterType) throws ToxicWaterException {
		if (isTheWaterClean == "Clean") {
			System.out.println("The " + name + " fish diving in clean " + waterType + "waters.");
			return waterType;
		} else {
			throw new ToxicWaterException("The water is " + isTheWaterClean + ". Fishes cannot dive there");
		}
	}
	

}
