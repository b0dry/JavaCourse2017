package com.verint.b0dry.homework2;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.verint.b0dry.aquarium.Aquarium;
import com.verint.b0dry.fishes.Fish;
import com.verint.b0dry.fishes.Gupies;
import com.verint.b0dry.fishes.Oscars;
import com.verint.b0dry.fishes.Sharks;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) throws Throwable {
		// System.out.println( "Hello World!" );
		Aquarium myAquarium = new Aquarium();
		String tempKind;
		String hasNext = "";

		// System.out.println("Please insert number of fishes you wish to add");
		Scanner in = new Scanner(System.in);
		List<Fish> fishes = new ArrayList<Fish>();
		int i = 0;
		do {
			// if (hasNext.equals("y")) {
			// }
			System.out.println("Please add fish number " + (i + 1) + " type, and hit Enter: ");
			tempKind = in.next();
			System.out.println("Please add fish name: ");
			System.out.println();
			switch (tempKind) {
			case "Oscar":
				fishes.add(new Oscars(in.next()));
				i++;
				System.out.println("Oscar fish is added");
				hasNext = in.toString();
				break;
			case "Shark":
				fishes.add(new Sharks(in.next()));
				i++;
				System.out.println("Shark fish is added");
				hasNext = in.toString();
				break;
			case "Gupi":
				fishes.add(new Gupies(in.next()));
				i++;
				System.out.println("Gupi fish is added");
				hasNext = in.toString();
				break;
			default:
				System.out.println("There is no such fish kind. Allowed kinds are: Oscar, Shark or Gupi");
				continue;
			}

			System.out.println("Do you want to add another fish?");
			hasNext = in.next();

		} while (!hasNext.equals("n"));
		in.close();
		myAquarium.checkFishes(fishes, "Clean");
	}
}
