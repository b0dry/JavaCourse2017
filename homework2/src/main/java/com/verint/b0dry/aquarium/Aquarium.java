package com.verint.b0dry.aquarium;

import java.util.List;

import com.verint.b0dry.customexceptions.ToxicWaterException;
import com.verint.b0dry.fishes.Fish;


public class Aquarium {
	public void checkFishes (List<Fish> fishes, String isTheWaterClean) throws ToxicWaterException{
		for (Fish tempFish: fishes) {
			try {
				tempFish.swim(tempFish.getFishName());
				tempFish.dive(isTheWaterClean);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}